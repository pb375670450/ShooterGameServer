# -*- coding: utf-8 -*-
import json
import logging
import atexit
from gevent import monkey; monkey.patch_all()
from gevent.server import DatagramServer, StreamServer
from gevent.pool import Pool

from util.config import ServerConfig, MongoDBConfig
from util.crash_on_ipy import *
from protobuf import shooter_game_pb2 as ShooterGame

# TODO: 为了方便暂时先用json作为载体， 之后可改用pb

server = None

def close():
  if server != None and not server.closed:
    server.close()


def connection_handler(socket, address):
  logging.debug("host {0} connected".format(address))
  socket.sendall("connected")
  for data in socket.makefile('r'):
    logging.debug("received %s" % data)
    socket.sendall("ok")


def main():
  global server
  atexit.register(close)
  logging.basicConfig(filename='game_server.log', level=logging.DEBUG, 
  format='%(asctime)s,%(msecs)d %(levelname)-8s [%(filename)s:%(lineno)d] %(message)s', datefmt='%d-%m-%Y:%H:%M:%S')

  conf = ServerConfig("./conf/conf.ini")
  pool = Pool(conf.game_max_conn)
  # server = GameServer((conf.host, conf.game_port), spawn=pool)
  server = StreamServer((conf.host, conf.game_port), connection_handler, spawn=pool)
  server.serve_forever()
  


if __name__ == "__main__":
  main()
