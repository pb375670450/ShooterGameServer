# -*- coding: utf-8 -*-
import json
import uuid
import mongoengine
from bottle import route, run, template, request

from models import *
from util.constants import *
from util.config import ServerConfig, MongoDBConfig


def build_response(code, msg = None):
  return {
    "code": code,
    "msg": msg
  }

def create_player(username, id):
  player = Player(username=username, playerId=id)
  player.save()

@route('/register', method='POST')
def register():
  username = request.json.get('username')
  password = request.json.get('password')
  if Account.objects(username__exists=username):
    return build_response(RegisterResponseCode.UsernameAlreadyExist)

  account = Account(username=username, password=password)
  account.save()
  return build_response(RegisterResponseCode.Success)


@route('/login', method='POST')
def login():
  username = request.json.get('username')
  password = request.json.get('password')
  accounts = Account.objects(username=username)
  if accounts.count() == 0:
    return build_response(LoginResponseCode.UserNotExist)

  account = accounts[0]
  if account.password != password:
    return build_response(LoginResponseCode.InvalidPassword)

  resp = build_response(LoginResponseCode.Success)
  resp["session"] = str(uuid.uuid4())
  conn = Connection(username=username, session=resp["session"])
  conn.save()
  return resp


def main():
  conf = ServerConfig("./conf/conf.ini")
  dbconf = MongoDBConfig("./conf/conf.ini")
  mongoengine.connect(dbconf.db, host=dbconf.host, port=dbconf.port)
  run(host=conf.host, port=conf.http_port)


if __name__ == "__main__":
  main()