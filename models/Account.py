# -*- coding: utf-8 -*-
from mongoengine import StringField, Document, DateTimeField
import datetime

class Account(Document):
  username = StringField(required=True, max_length=50, unnique=True, primary_key=True)
  password = StringField(required=True, max_length=50)
  create_time = DateTimeField(default=datetime.datetime.utcnow)
  status = StringField(required=True, default="normal")

