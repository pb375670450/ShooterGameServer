# -*- coding: utf-8 -*-
from mongoengine import StringField, Document, DateTimeField
import datetime

class Connection(Document):
  username = StringField(required=True, max_length=50, unnique=True, primary_key=True)
  session = StringField(required=True)
  ip = StringField()
  update_time = DateTimeField(default=datetime.datetime.utcnow)