# -*- coding: utf-8 -*-
from mongoengine import StringField, Document, DateTimeField, IntField, DictField

class Monster(Document):
  monsterId = StringField(required=True, unique=True)
  health = IntField(default=100)
  transform = DictField()
  
