# -*- coding: utf-8 -*-
from mongoengine import StringField, Document, DateTimeField, IntField, DictField

class Player(Document):
  username = StringField(required=True, unique=True)
  playerId = StringField(required=True, max_length=20, unique=True, primary_key=True)
  level = IntField(default=1)
  health = IntField(default=100)
  money = IntField(default=0)
  transform = DictField()

