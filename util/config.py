# -*- coding: utf-8 -*-
import ConfigParser

import os

class BaseConfig():
  

  def __init__(self, conf_path):
    self.conf_path = conf_path
    self.cf = ConfigParser.ConfigParser()
    self.load_conf()


  def load_conf(self):
    self.cf.read(os.path.realpath(self.conf_path))

  
  def reload(self):
    self.load_conf()

  
  def get(self, section, option, default=None):
    try:
      return self.cf.get(section, option)
    except ConfigParser.Error as e:
      if default is None or not isinstance(default, str):
        raise e
      else:
        return default


  def getint(self, section, option, default=None):
    try:
      return self.cf.getint(section, option)
    except ConfigParser.Error, e:
      if default is None or not isinstance(default, int):
        raise e
      else:
        return default


  def getlist(self, section, option, default=None):
    try:
      s = self.cf.get(section, option)
      return [chunk.strip() for chunk in s.split(',')]
    except ConfigParser.Error, e:
      if default is None or not isinstance(default, list):
        raise e
      else:
        return default
    

class ServerConfig(BaseConfig):
  @property
  def host(self):
    return self.get("common", "host", "0.0.0.0")

  @property
  def http_port(self):
    return self.getint("http_server", "port", 8088)

  @property
  def game_port(self):
    return self.getint("game_server", "port", 8089)
  
  @property
  def game_max_conn(self):
    return self.getint("game_server", "max_connection", 1000)


class MongoDBConfig(BaseConfig):
  @property
  def host(self):
    return self.get("mongodb", "host", "127.0.0.1")

  @property
  def port(self):
    return self.getint("mongodb", "port", 27017)

  @property
  def db(self):
    return self.get("mongodb", "db")
  