# -*- coding: utf-8 -*-

class ResponseCode():
  Success = "SUCCESS"
  InternalError = "INTERNAL_ERROR"
  InvalidParameter = "INVALID_PARAMETER"

class RegisterResponseCode(ResponseCode):
  UsernameAlreadyExist = "USERNAME_ALREADY_EXIST"

class LoginResponseCode(ResponseCode):
  UserNotExist = "USER_NOT_EXIST"
  InvalidPassword = "INVALID_PASSWORD"

class GameSettings():
  pass
  
